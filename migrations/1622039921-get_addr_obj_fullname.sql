create or replace function get_addr_obj_fullname(vobjectid bigint, vhierarchy char(3)) returns varchar as $$
declare
  vtypename varchar(50);
  vfullname varchar;
begin
  while (vobjectid is not null and vobjectid != 0) loop
    select concat(name, ', ', vfullname), typename
    into vfullname, vtypename
    from as_addr_obj
    where objectid = vobjectid and isactual = true and isactive = true
    limit 1;

    exit when vfullname is null;

    select concat(lower(name), ' ', vfullname)
    into vfullname
    from as_addr_obj_types
    where shortname = vtypename
    limit 1;

    if (vhierarchy = 'mun') then
      select parentobjid
      into vobjectid
      from as_mun_hierarchy
      where objectid = vobjectid and isactive = true
      limit 1;
    else
      select parentobjid
      into vobjectid
      from as_adm_hierarchy
      where objectid = vobjectid and isactive = true
      limit 1;
    end if;
  end loop;

  return replace(replace(rtrim(vfullname, ', '), 'Ё', 'Е'), 'ё', 'е');
end
$$ language plpgsql;
