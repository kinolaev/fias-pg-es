create or replace function get_house_postfix(vid bigint) returns varchar as $$
declare
  vhousetype int;
  vhousenum varchar(50);
  vaddtype1 int;
  vaddnum1 varchar(50);
  vaddtype2 int;
  vaddnum2 varchar(50);
  vhousepostfix varchar;
begin

  select housetype, housenum, addtype1, addnum1, addtype2, addnum2
  into vhousetype, vhousenum, vaddtype1, vaddnum1, vaddtype2, vaddnum2
  from as_houses
  where id = vid
  limit 1;

  if (vhousetype is not null) then
    select concat(name, ' ', vhousenum)
    into vhousepostfix
    from as_house_types
    where id = vhousetype
    limit 1;
  end if;

  if (vaddtype1 is not null) then
    select concat(vhousepostfix, ' ', name, ' ', vaddnum1)
    into vhousepostfix
    from as_addhouse_types
    where id = vaddtype1
    limit 1;
  end if;

  if (vaddtype2 is not null) then
    select concat(vhousepostfix, ' ', name, ' ', vaddnum2)
    into vhousepostfix
    from as_addhouse_types
    where id = vaddtype2
    limit 1;
  end if;

  return ltrim(vhousepostfix);
end
$$ language plpgsql;
