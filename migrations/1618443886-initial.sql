create table if not exists lock (
  id integer primary key,
  created_at timestamp with time zone not null default now()
);

create table if not exists log_versions (
  id serial primary key,
  version_id integer not null,
  text_version text not null,
  date text not null,
  url text not null,
  last_modified timestamp with time zone not null,
  content_length bigint not null,
  created_at timestamp with time zone not null default now(),
  loaded_at timestamp with time zone
);

create table if not exists log_files (
  log_version_id integer not null,
  path text not null,
  size bigint not null,
  created_at timestamp with time zone not null default now()
);
