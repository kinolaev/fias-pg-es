import fs from "fs"
import { getGARSchemas } from "./nalog"
import { GARType, typeFromXsd } from "./gar"

export async function generate(): Promise<void> {
  const zip = await getGARSchemas()
  const types: Record<string, GARType> = {}
  for (const file of zip.files) {
    const type = await typeFromXsd(file.path, await file.buffer())
    types[type.name] = type
  }
  const data = [
    'import type { GARType } from "./gar"',
    "const types: Record<string, GARType> = " + JSON.stringify(types, null, 2),
    "export default types",
  ]
  await fs.promises.writeFile("src/gar.types.g.ts", data.join("\n\n"))
}
