import type { GARType } from "./gar"
import types from "./gar.types.g"

export class Table {
  public type: GARType
  constructor(
    public prefix: string,
    typeName: string = prefix,
    public key: string = "ID",
    public indices: Array<string> = [],
    public foreign: Array<Foreign> = []
  ) {
    const type = types[typeName]
    if (type === undefined) throw new Error("Unknown type " + typeName)
    this.type = type
  }
}

class Foreign {
  constructor(public key: string, public ref: { table: string; key: string }) {}
}

const tables: Array<Table> = [
  new Table("AS_HOUSE_TYPES"),
  new Table("AS_ADDHOUSE_TYPES", "AS_HOUSE_TYPES"),
  new Table("AS_HOUSES", "AS_HOUSES", "ID", ["OBJECTID", "OBJECTGUID"]),
  new Table("AS_MUN_HIERARCHY"),
  new Table("AS_NORMATIVE_DOCS"),
  new Table("AS_NORMATIVE_DOCS_KINDS"),
  new Table("AS_NORMATIVE_DOCS_TYPES"),
  new Table("AS_OBJECT_LEVELS", "AS_OBJECT_LEVELS", "LEVEL"),
  new Table("AS_OPERATION_TYPES"),
  new Table("AS_ADDR_OBJ_PARAMS", "AS_PARAM", "ID", ["OBJECTID"]),
  new Table("AS_HOUSES_PARAMS", "AS_PARAM", "ID", ["OBJECTID"]),
  new Table("AS_APARTMENTS_PARAMS", "AS_PARAM"),
  new Table("AS_ROOMS_PARAMS", "AS_PARAM"),
  new Table("AS_STEADS_PARAMS", "AS_PARAM"),
  new Table("AS_CARPLACES_PARAMS", "AS_PARAM"),
  new Table("AS_PARAM_TYPES"),
  new Table("AS_REESTR_OBJECTS"),
  new Table("AS_ROOM_TYPES"),
  new Table("AS_ROOMS"),
  new Table("AS_STEADS"),
  new Table("AS_ADDR_OBJ", "AS_ADDR_OBJ", "ID", ["OBJECTID", "OBJECTGUID"]),
  new Table("AS_ADDR_OBJ_DIVISION"),
  new Table("AS_ADDR_OBJ_TYPES"),
  new Table("AS_ADM_HIERARCHY", "AS_ADM_HIERARCHY", "ID", ["OBJECTID", "PARENTOBJID"]),
  new Table("AS_APARTMENT_TYPES"),
  new Table("AS_APARTMENTS"),
  new Table("AS_CARPLACES"),
  new Table("AS_CHANGE_HISTORY"),
]

export default tables
