import type { Pool } from "pg"
import type { Client as ESClient } from "./elastic"

import * as q from "./db/queries"
import { DownloadFileInfo, getAllDownloadFileInfo } from "./nalog"
import { tables } from "./gar"
import { load, cleanup } from "./load"
import { fullIndex } from "./full"
import { retry } from "./utils"

export async function delta(
  pg: Pool,
  es: ESClient,
  version: null | number,
  prefixes: Array<string>,
  regions: null | Array<string> = null,
  cache: boolean = false
): Promise<void> {
  const localLastVersion = await q.lastLoadedVersion(pg)

  const allVersions = await getAllDownloadFileInfo()
  if (allVersions.length === 0) {
    throw new Error("Empty GetAllDownloadFileInfo list")
  }

  const remoteVersionIndex =
    version === null ? 0 : allVersions.findIndex((v) => v.VersionId === version)
  if (remoteVersionIndex === -1) {
    const msg = `Version ${version} is not found in GetAllDownloadFileInfo list`
    throw new Error(msg)
  }

  const remoteLastVersionInfo = allVersions[remoteVersionIndex]!
  if (localLastVersion.version_id === remoteLastVersionInfo.VersionId) return

  const localVersionIndex = allVersions.findIndex(
    (v) => localLastVersion.version_id === v.VersionId
  )
  if (localVersionIndex === -1) {
    const msg = `Local version ${localLastVersion.version_id} is not found in GetAllDownloadFileInfo list`
    throw new Error(msg)
  }
  if (localVersionIndex < remoteVersionIndex) {
    const msg = `Local version ${remoteLastVersionInfo.VersionId} is newer than ${remoteLastVersionInfo.VersionId}`
    throw new Error(msg)
  }

  const missingVersions = allVersions
    .slice(remoteVersionIndex, localVersionIndex)
    .reverse()

  const nextPrefix = "next"
  let error: void | Error
  let updated = false
  for (const version of missingVersions) {
    if (version.GarXMLFullURL === "" && version.GarXMLDeltaURL === "") continue
    const exec = () =>
      deltaVersion(pg, version, nextPrefix, prefixes, regions, cache)
    error = await retry(3, exec).catch((error: Error) => error)
    if (error !== undefined) {
      console.log(new Date().toISOString(), version.VersionId, error.message)
      break
    }
    updated = true
  }

  if (error === undefined) {
    await cleanup(pg, nextPrefix, prefixes)
  }

  if (updated) {
    // TODO: index only affected rows
    await fullIndex(pg, es)
  }
}

async function deltaVersion(
  pg: Pool,
  info: DownloadFileInfo,
  nextPrefix: string,
  prefixes: Array<string>,
  regions: null | Array<string> = null,
  cache = false
): Promise<void> {
  console.log(new Date().toISOString(), "loading delta", info.VersionId)
  const versionId = await load(
    pg,
    info,
    info.GarXMLDeltaURL,
    prefixes,
    regions,
    cache,
    nextPrefix
  )

  for (const prefix of prefixes) {
    const table = tables.find((t) => t.prefix === prefix)
    if (table === undefined) throw new Error("Unexpected prefix " + prefix)

    const mainTable = prefix.toLowerCase()
    const nextTable = [nextPrefix, mainTable].join("_")
    const primaryKey = table.key.toLowerCase()
    const fields = table.type.fields.map((f) => f.name.toLowerCase())
    const fieldList = fields.join('", "')
    const updateList = fields
      .map((name) => `"${name}" = excluded."${name}"`)
      .join(", ")
    console.log(new Date().toISOString(), "merging", nextTable)
    await pg.query(
      `insert into ${mainTable} ("${fieldList}") select "${fieldList}" from ${nextTable} on conflict ("${primaryKey}") do update set ${updateList}`
    )
  }

  await q.versionLoaded(pg, versionId)
}
