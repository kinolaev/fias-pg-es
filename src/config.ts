export const prefixes = [
  "AS_ADDR_OBJ_TYPES",
  "AS_HOUSE_TYPES",
  "AS_ADDHOUSE_TYPES",
  "AS_OBJECT_LEVELS",
  "AS_OPERATION_TYPES",
  "AS_PARAM_TYPES",

  "AS_ADDR_OBJ",
  "AS_ADDR_OBJ_PARAMS",
  "AS_HOUSES",
  "AS_HOUSES_PARAMS",
  "AS_ADM_HIERARCHY",
]
export const hierarchies = ["adm"]
