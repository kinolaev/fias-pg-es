import { Client } from "./elastic"

const defaultUrl = "http://localhost:9200"

export const client = new Client(process.env.ELASTICSEARCH_URL ?? defaultUrl)
