import { Duplex } from "stream"
import { SaxesParser } from "saxes"
import { GARType } from "./gar"

export class GARObjectStream extends Duplex {
  private parser: SaxesParser

  constructor(private type: GARType) {
    super({ decodeStrings: false, readableObjectMode: true })

    this.parser = new SaxesParser()
    this.parser.on("error", (error) => {
      this.destroy(error)
    })
    this.parser.on("opentag", (tag) => {
      if (tag.name !== this.type.item) return
      for (const field of this.type.fields) {
        if (
          field.required &&
          field.type === "xs:string" &&
          tag.attributes[field.name] === undefined
        ) {
          tag.attributes[field.name] = ""
        }
      }
      this.push(tag.attributes)
    })
    this.parser.on("end", () => {
      this.push(null)
    })
  }

  _write(
    chunk: Buffer | string,
    encoding: BufferEncoding,
    callback: (error?: Error | null) => void
  ): void {
    try {
      this.parser.write(chunk)
      callback(null)
    } catch (error) {
      // @ts-ignore
      callback(error)
    }
  }
  _final(callback: (error?: Error | null) => void): void {
    this._destroy(null, callback)
  }

  _read(size: number): void {}

  _destroy(error: Error | null, callback: (error: Error | null) => void): void {
    this.parser.close()
    callback(error)
  }
}
