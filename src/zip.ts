import type { Readable } from "stream"
import type { CentralDirectory } from "unzipper"
/** @ts-ignore */
import centralDirectory from "unzipper/lib/Open/directory"

export default centralDirectory as (
  source: {
    stream(offset: number, length?: number): Readable
    size(): Promise<number>
  },
  options?: { tailSize?: number; crx?: boolean }
) => Promise<CentralDirectory>
