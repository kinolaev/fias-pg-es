import type { GARType } from "./gar"

const types: Record<string, GARType> = {
  "AS_HOUSE_TYPES": {
    "path": "AS_HOUSE_TYPES_2_251_13_04_01_01.xsd",
    "name": "AS_HOUSE_TYPES",
    "part": "251_13",
    "format": "04_01_01",
    "list": "HOUSETYPES",
    "item": "HOUSETYPE",
    "fields": [
      {
        "name": "ID",
        "type": "xs:integer",
        "required": true
      },
      {
        "name": "NAME",
        "type": "xs:string",
        "required": true,
        "maxLength": "50"
      },
      {
        "name": "SHORTNAME",
        "type": "xs:string",
        "required": false,
        "maxLength": "50"
      },
      {
        "name": "DESC",
        "type": "xs:string",
        "required": false,
        "maxLength": "250"
      },
      {
        "name": "UPDATEDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "STARTDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "ENDDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "ISACTIVE",
        "type": "xs:boolean",
        "required": true
      }
    ]
  },
  "AS_HOUSES": {
    "path": "AS_HOUSES_2_251_08_04_01_01.xsd",
    "name": "AS_HOUSES",
    "part": "251_08",
    "format": "04_01_01",
    "list": "HOUSES",
    "item": "HOUSE",
    "fields": [
      {
        "name": "ID",
        "type": "xs:long",
        "required": true
      },
      {
        "name": "OBJECTID",
        "type": "xs:long",
        "required": true
      },
      {
        "name": "OBJECTGUID",
        "type": "xs:string",
        "required": true,
        "length": "36"
      },
      {
        "name": "CHANGEID",
        "type": "xs:long",
        "required": true
      },
      {
        "name": "HOUSENUM",
        "type": "xs:string",
        "required": false,
        "maxLength": "50"
      },
      {
        "name": "ADDNUM1",
        "type": "xs:string",
        "required": false,
        "maxLength": "50"
      },
      {
        "name": "ADDNUM2",
        "type": "xs:string",
        "required": false,
        "maxLength": "50"
      },
      {
        "name": "HOUSETYPE",
        "type": "xs:integer",
        "required": false
      },
      {
        "name": "ADDTYPE1",
        "type": "xs:integer",
        "required": false
      },
      {
        "name": "ADDTYPE2",
        "type": "xs:integer",
        "required": false
      },
      {
        "name": "OPERTYPEID",
        "type": "xs:integer",
        "required": true
      },
      {
        "name": "PREVID",
        "type": "xs:long",
        "required": false
      },
      {
        "name": "NEXTID",
        "type": "xs:long",
        "required": false
      },
      {
        "name": "UPDATEDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "STARTDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "ENDDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "ISACTUAL",
        "type": "xs:boolean",
        "required": true
      },
      {
        "name": "ISACTIVE",
        "type": "xs:boolean",
        "required": true
      }
    ]
  },
  "AS_MUN_HIERARCHY": {
    "path": "AS_MUN_HIERARCHY_2_251_10_04_01_01.xsd",
    "name": "AS_MUN_HIERARCHY",
    "part": "251_10",
    "format": "04_01_01",
    "list": "ITEMS",
    "item": "ITEM",
    "fields": [
      {
        "name": "ID",
        "type": "xs:long",
        "required": true
      },
      {
        "name": "OBJECTID",
        "type": "xs:long",
        "required": true
      },
      {
        "name": "PARENTOBJID",
        "type": "xs:long",
        "required": false
      },
      {
        "name": "CHANGEID",
        "type": "xs:long",
        "required": true
      },
      {
        "name": "OKTMO",
        "type": "xs:string",
        "required": false,
        "maxLength": "11"
      },
      {
        "name": "PREVID",
        "type": "xs:long",
        "required": false
      },
      {
        "name": "NEXTID",
        "type": "xs:long",
        "required": false
      },
      {
        "name": "UPDATEDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "STARTDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "ENDDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "ISACTIVE",
        "type": "xs:boolean",
        "required": true
      }
    ]
  },
  "AS_NORMATIVE_DOCS": {
    "path": "AS_NORMATIVE_DOCS_2_251_11_04_01_01.xsd",
    "name": "AS_NORMATIVE_DOCS",
    "part": "251_11",
    "format": "04_01_01",
    "list": "NORMDOCS",
    "item": "NORMDOC",
    "fields": [
      {
        "name": "ID",
        "type": "xs:long",
        "required": true
      },
      {
        "name": "NAME",
        "type": "xs:string",
        "required": true,
        "maxLength": "8000"
      },
      {
        "name": "DATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "NUMBER",
        "type": "xs:string",
        "required": true,
        "maxLength": "150"
      },
      {
        "name": "TYPE",
        "type": "xs:integer",
        "required": true
      },
      {
        "name": "KIND",
        "type": "xs:integer",
        "required": true
      },
      {
        "name": "UPDATEDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "ORGNAME",
        "type": "xs:string",
        "required": false,
        "maxLength": "255"
      },
      {
        "name": "REGNUM",
        "type": "xs:string",
        "required": false,
        "maxLength": "100"
      },
      {
        "name": "REGDATE",
        "type": "xs:date",
        "required": false
      },
      {
        "name": "ACCDATE",
        "type": "xs:date",
        "required": false
      },
      {
        "name": "COMMENT",
        "type": "xs:string",
        "required": false,
        "maxLength": "8000"
      }
    ]
  },
  "AS_NORMATIVE_DOCS_KINDS": {
    "path": "AS_NORMATIVE_DOCS_KINDS_2_251_09_04_01_01.xsd",
    "name": "AS_NORMATIVE_DOCS_KINDS",
    "part": "251_09",
    "format": "04_01_01",
    "list": "NDOCKINDS",
    "item": "NDOCKIND",
    "fields": [
      {
        "name": "ID",
        "type": "xs:integer",
        "required": true
      },
      {
        "name": "NAME",
        "type": "xs:string",
        "required": true,
        "maxLength": "500"
      }
    ]
  },
  "AS_NORMATIVE_DOCS_TYPES": {
    "path": "AS_NORMATIVE_DOCS_TYPES_2_251_16_04_01_01.xsd",
    "name": "AS_NORMATIVE_DOCS_TYPES",
    "part": "251_16",
    "format": "04_01_01",
    "list": "NDOCTYPES",
    "item": "NDOCTYPE",
    "fields": [
      {
        "name": "ID",
        "type": "xs:integer",
        "required": true
      },
      {
        "name": "NAME",
        "type": "xs:string",
        "required": true,
        "maxLength": "500"
      },
      {
        "name": "STARTDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "ENDDATE",
        "type": "xs:date",
        "required": true
      }
    ]
  },
  "AS_OBJECT_LEVELS": {
    "path": "AS_OBJECT_LEVELS_2_251_12_04_01_01.xsd",
    "name": "AS_OBJECT_LEVELS",
    "part": "251_12",
    "format": "04_01_01",
    "list": "OBJECTLEVELS",
    "item": "OBJECTLEVEL",
    "fields": [
      {
        "name": "LEVEL",
        "type": "xs:integer",
        "required": true
      },
      {
        "name": "NAME",
        "type": "xs:string",
        "required": true,
        "maxLength": "250"
      },
      {
        "name": "SHORTNAME",
        "type": "xs:string",
        "required": false,
        "maxLength": "50"
      },
      {
        "name": "UPDATEDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "STARTDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "ENDDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "ISACTIVE",
        "type": "xs:boolean",
        "required": true
      }
    ]
  },
  "AS_OPERATION_TYPES": {
    "path": "AS_OPERATION_TYPES_2_251_14_04_01_01.xsd",
    "name": "AS_OPERATION_TYPES",
    "part": "251_14",
    "format": "04_01_01",
    "list": "OPERATIONTYPES",
    "item": "OPERATIONTYPE",
    "fields": [
      {
        "name": "ID",
        "type": "xs:integer",
        "required": true
      },
      {
        "name": "NAME",
        "type": "xs:string",
        "required": true,
        "maxLength": "100"
      },
      {
        "name": "SHORTNAME",
        "type": "xs:string",
        "required": false,
        "maxLength": "100"
      },
      {
        "name": "DESC",
        "type": "xs:string",
        "required": false,
        "maxLength": "250"
      },
      {
        "name": "UPDATEDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "STARTDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "ENDDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "ISACTIVE",
        "type": "xs:boolean",
        "required": true
      }
    ]
  },
  "AS_PARAM": {
    "path": "AS_PARAM_2_251_02_04_01_01.xsd",
    "name": "AS_PARAM",
    "part": "251_02",
    "format": "04_01_01",
    "list": "PARAMS",
    "item": "PARAM",
    "fields": [
      {
        "name": "ID",
        "type": "xs:long",
        "required": true
      },
      {
        "name": "OBJECTID",
        "type": "xs:long",
        "required": true
      },
      {
        "name": "CHANGEID",
        "type": "xs:long",
        "required": false
      },
      {
        "name": "CHANGEIDEND",
        "type": "xs:long",
        "required": true
      },
      {
        "name": "TYPEID",
        "type": "xs:integer",
        "required": true
      },
      {
        "name": "VALUE",
        "type": "xs:string",
        "required": true,
        "maxLength": "8000"
      },
      {
        "name": "UPDATEDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "STARTDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "ENDDATE",
        "type": "xs:date",
        "required": true
      }
    ]
  },
  "AS_PARAM_TYPES": {
    "path": "AS_PARAM_TYPES_2_251_20_04_01_01.xsd",
    "name": "AS_PARAM_TYPES",
    "part": "251_20",
    "format": "04_01_01",
    "list": "PARAMTYPES",
    "item": "PARAMTYPE",
    "fields": [
      {
        "name": "ID",
        "type": "xs:integer",
        "required": true
      },
      {
        "name": "NAME",
        "type": "xs:string",
        "required": true,
        "maxLength": "50"
      },
      {
        "name": "CODE",
        "type": "xs:string",
        "required": true,
        "maxLength": "50"
      },
      {
        "name": "DESC",
        "type": "xs:string",
        "required": false,
        "maxLength": "120"
      },
      {
        "name": "UPDATEDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "STARTDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "ENDDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "ISACTIVE",
        "type": "xs:boolean",
        "required": true
      }
    ]
  },
  "AS_REESTR_OBJECTS": {
    "path": "AS_REESTR_OBJECTS_2_251_22_04_01_01.xsd",
    "name": "AS_REESTR_OBJECTS",
    "part": "251_22",
    "format": "04_01_01",
    "list": "REESTR_OBJECTS",
    "item": "OBJECT",
    "fields": [
      {
        "name": "OBJECTID",
        "type": "xs:long",
        "required": true
      },
      {
        "name": "CREATEDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "CHANGEID",
        "type": "xs:long",
        "required": true
      },
      {
        "name": "LEVELID",
        "type": "xs:integer",
        "required": true
      },
      {
        "name": "UPDATEDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "OBJECTGUID",
        "type": "xs:string",
        "required": true,
        "length": "36"
      },
      {
        "name": "ISACTIVE",
        "type": "xs:boolean",
        "required": true
      }
    ]
  },
  "AS_ROOM_TYPES": {
    "path": "AS_ROOM_TYPES_2_251_17_04_01_01.xsd",
    "name": "AS_ROOM_TYPES",
    "part": "251_17",
    "format": "04_01_01",
    "list": "ROOMTYPES",
    "item": "ROOMTYPE",
    "fields": [
      {
        "name": "ID",
        "type": "xs:integer",
        "required": true
      },
      {
        "name": "NAME",
        "type": "xs:string",
        "required": true,
        "maxLength": "100"
      },
      {
        "name": "SHORTNAME",
        "type": "xs:string",
        "required": false,
        "maxLength": "50"
      },
      {
        "name": "DESC",
        "type": "xs:string",
        "required": false,
        "maxLength": "250"
      },
      {
        "name": "UPDATEDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "STARTDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "ENDDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "ISACTIVE",
        "type": "xs:boolean",
        "required": true
      }
    ]
  },
  "AS_ROOMS": {
    "path": "AS_ROOMS_2_251_15_04_01_01.xsd",
    "name": "AS_ROOMS",
    "part": "251_15",
    "format": "04_01_01",
    "list": "ROOMS",
    "item": "ROOM",
    "fields": [
      {
        "name": "ID",
        "type": "xs:long",
        "required": true
      },
      {
        "name": "OBJECTID",
        "type": "xs:long",
        "required": true
      },
      {
        "name": "OBJECTGUID",
        "type": "xs:string",
        "required": true,
        "length": "36"
      },
      {
        "name": "CHANGEID",
        "type": "xs:long",
        "required": true
      },
      {
        "name": "NUMBER",
        "type": "xs:string",
        "required": true,
        "maxLength": "50"
      },
      {
        "name": "ROOMTYPE",
        "type": "xs:integer",
        "required": true
      },
      {
        "name": "OPERTYPEID",
        "type": "xs:integer",
        "required": true
      },
      {
        "name": "PREVID",
        "type": "xs:long",
        "required": false
      },
      {
        "name": "NEXTID",
        "type": "xs:long",
        "required": false
      },
      {
        "name": "UPDATEDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "STARTDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "ENDDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "ISACTUAL",
        "type": "xs:boolean",
        "required": true
      },
      {
        "name": "ISACTIVE",
        "type": "xs:boolean",
        "required": true
      }
    ]
  },
  "AS_STEADS": {
    "path": "AS_STEADS_2_251_18_04_01_01.xsd",
    "name": "AS_STEADS",
    "part": "251_18",
    "format": "04_01_01",
    "list": "STEADS",
    "item": "STEAD",
    "fields": [
      {
        "name": "ID",
        "type": "xs:integer",
        "required": true
      },
      {
        "name": "OBJECTID",
        "type": "xs:integer",
        "required": true
      },
      {
        "name": "OBJECTGUID",
        "type": "xs:string",
        "required": true,
        "length": "36"
      },
      {
        "name": "CHANGEID",
        "type": "xs:integer",
        "required": true
      },
      {
        "name": "NUMBER",
        "type": "xs:string",
        "required": true,
        "maxLength": "250"
      },
      {
        "name": "OPERTYPEID",
        "type": "xs:string",
        "required": true,
        "maxLength": "2"
      },
      {
        "name": "PREVID",
        "type": "xs:integer",
        "required": false
      },
      {
        "name": "NEXTID",
        "type": "xs:integer",
        "required": false
      },
      {
        "name": "UPDATEDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "STARTDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "ENDDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "ISACTUAL",
        "type": "xs:boolean",
        "required": true
      },
      {
        "name": "ISACTIVE",
        "type": "xs:boolean",
        "required": true
      }
    ]
  },
  "AS_ADDR_OBJ": {
    "path": "AS_ADDR_OBJ_2_251_01_04_01_01.xsd",
    "name": "AS_ADDR_OBJ",
    "part": "251_01",
    "format": "04_01_01",
    "list": "ADDRESSOBJECTS",
    "item": "OBJECT",
    "fields": [
      {
        "name": "ID",
        "type": "xs:long",
        "required": true
      },
      {
        "name": "OBJECTID",
        "type": "xs:long",
        "required": true
      },
      {
        "name": "OBJECTGUID",
        "type": "xs:string",
        "required": true,
        "length": "36"
      },
      {
        "name": "CHANGEID",
        "type": "xs:long",
        "required": true
      },
      {
        "name": "NAME",
        "type": "xs:string",
        "required": true,
        "maxLength": "250"
      },
      {
        "name": "TYPENAME",
        "type": "xs:string",
        "required": true,
        "maxLength": "50"
      },
      {
        "name": "LEVEL",
        "type": "xs:integer",
        "required": true
      },
      {
        "name": "OPERTYPEID",
        "type": "xs:integer",
        "required": true
      },
      {
        "name": "PREVID",
        "type": "xs:long",
        "required": false
      },
      {
        "name": "NEXTID",
        "type": "xs:long",
        "required": false
      },
      {
        "name": "UPDATEDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "STARTDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "ENDDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "ISACTUAL",
        "type": "xs:boolean",
        "required": true
      },
      {
        "name": "ISACTIVE",
        "type": "xs:boolean",
        "required": true
      }
    ]
  },
  "AS_ADDR_OBJ_DIVISION": {
    "path": "AS_ADDR_OBJ_DIVISION_2_251_19_04_01_01.xsd",
    "name": "AS_ADDR_OBJ_DIVISION",
    "part": "251_19",
    "format": "04_01_01",
    "list": "ITEMS",
    "item": "ITEM",
    "fields": [
      {
        "name": "ID",
        "type": "xs:long",
        "required": true
      },
      {
        "name": "PARENTID",
        "type": "xs:long",
        "required": true
      },
      {
        "name": "CHILDID",
        "type": "xs:long",
        "required": true
      },
      {
        "name": "CHANGEID",
        "type": "xs:long",
        "required": true
      }
    ]
  },
  "AS_ADDR_OBJ_TYPES": {
    "path": "AS_ADDR_OBJ_TYPES_2_251_03_04_01_01.xsd",
    "name": "AS_ADDR_OBJ_TYPES",
    "part": "251_03",
    "format": "04_01_01",
    "list": "ADDRESSOBJECTTYPES",
    "item": "ADDRESSOBJECTTYPE",
    "fields": [
      {
        "name": "ID",
        "type": "xs:integer",
        "required": true
      },
      {
        "name": "LEVEL",
        "type": "xs:integer",
        "required": true
      },
      {
        "name": "SHORTNAME",
        "type": "xs:string",
        "required": true,
        "maxLength": "50"
      },
      {
        "name": "NAME",
        "type": "xs:string",
        "required": true,
        "maxLength": "250"
      },
      {
        "name": "DESC",
        "type": "xs:string",
        "required": false,
        "maxLength": "250"
      },
      {
        "name": "UPDATEDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "STARTDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "ENDDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "ISACTIVE",
        "type": "xs:boolean",
        "required": true
      }
    ]
  },
  "AS_ADM_HIERARCHY": {
    "path": "AS_ADM_HIERARCHY_2_251_04_04_01_01.xsd",
    "name": "AS_ADM_HIERARCHY",
    "part": "251_04",
    "format": "04_01_01",
    "list": "ITEMS",
    "item": "ITEM",
    "fields": [
      {
        "name": "ID",
        "type": "xs:long",
        "required": true
      },
      {
        "name": "OBJECTID",
        "type": "xs:long",
        "required": true
      },
      {
        "name": "PARENTOBJID",
        "type": "xs:long",
        "required": false
      },
      {
        "name": "CHANGEID",
        "type": "xs:long",
        "required": true
      },
      {
        "name": "REGIONCODE",
        "type": "xs:string",
        "required": false,
        "maxLength": "4"
      },
      {
        "name": "AREACODE",
        "type": "xs:string",
        "required": false,
        "maxLength": "4"
      },
      {
        "name": "CITYCODE",
        "type": "xs:string",
        "required": false,
        "maxLength": "4"
      },
      {
        "name": "PLACECODE",
        "type": "xs:string",
        "required": false,
        "maxLength": "4"
      },
      {
        "name": "PLANCODE",
        "type": "xs:string",
        "required": false,
        "maxLength": "4"
      },
      {
        "name": "STREETCODE",
        "type": "xs:string",
        "required": false,
        "maxLength": "4"
      },
      {
        "name": "PREVID",
        "type": "xs:long",
        "required": false
      },
      {
        "name": "NEXTID",
        "type": "xs:long",
        "required": false
      },
      {
        "name": "UPDATEDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "STARTDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "ENDDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "ISACTIVE",
        "type": "xs:boolean",
        "required": true
      }
    ]
  },
  "AS_APARTMENT_TYPES": {
    "path": "AS_APARTMENT_TYPES_2_251_07_04_01_01.xsd",
    "name": "AS_APARTMENT_TYPES",
    "part": "251_07",
    "format": "04_01_01",
    "list": "APARTMENTTYPES",
    "item": "APARTMENTTYPE",
    "fields": [
      {
        "name": "ID",
        "type": "xs:integer",
        "required": true
      },
      {
        "name": "NAME",
        "type": "xs:string",
        "required": true,
        "maxLength": "50"
      },
      {
        "name": "SHORTNAME",
        "type": "xs:string",
        "required": false,
        "maxLength": "50"
      },
      {
        "name": "DESC",
        "type": "xs:string",
        "required": false,
        "maxLength": "250"
      },
      {
        "name": "UPDATEDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "STARTDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "ENDDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "ISACTIVE",
        "type": "xs:boolean",
        "required": true
      }
    ]
  },
  "AS_APARTMENTS": {
    "path": "AS_APARTMENTS_2_251_05_04_01_01.xsd",
    "name": "AS_APARTMENTS",
    "part": "251_05",
    "format": "04_01_01",
    "list": "APARTMENTS",
    "item": "APARTMENT",
    "fields": [
      {
        "name": "ID",
        "type": "xs:long",
        "required": true
      },
      {
        "name": "OBJECTID",
        "type": "xs:long",
        "required": true
      },
      {
        "name": "OBJECTGUID",
        "type": "xs:string",
        "required": true,
        "length": "36"
      },
      {
        "name": "CHANGEID",
        "type": "xs:long",
        "required": true
      },
      {
        "name": "NUMBER",
        "type": "xs:string",
        "required": true,
        "maxLength": "50"
      },
      {
        "name": "APARTTYPE",
        "type": "xs:integer",
        "required": true
      },
      {
        "name": "OPERTYPEID",
        "type": "xs:long",
        "required": true
      },
      {
        "name": "PREVID",
        "type": "xs:long",
        "required": false
      },
      {
        "name": "NEXTID",
        "type": "xs:long",
        "required": false
      },
      {
        "name": "UPDATEDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "STARTDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "ENDDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "ISACTUAL",
        "type": "xs:boolean",
        "required": true
      },
      {
        "name": "ISACTIVE",
        "type": "xs:boolean",
        "required": true
      }
    ]
  },
  "AS_CARPLACES": {
    "path": "AS_CARPLACES_2_251_06_04_01_01.xsd",
    "name": "AS_CARPLACES",
    "part": "251_06",
    "format": "04_01_01",
    "list": "CARPLACES",
    "item": "CARPLACE",
    "fields": [
      {
        "name": "ID",
        "type": "xs:long",
        "required": true
      },
      {
        "name": "OBJECTID",
        "type": "xs:long",
        "required": true
      },
      {
        "name": "OBJECTGUID",
        "type": "xs:string",
        "required": true,
        "length": "36"
      },
      {
        "name": "CHANGEID",
        "type": "xs:long",
        "required": true
      },
      {
        "name": "NUMBER",
        "type": "xs:string",
        "required": true,
        "maxLength": "50"
      },
      {
        "name": "OPERTYPEID",
        "type": "xs:integer",
        "required": true
      },
      {
        "name": "PREVID",
        "type": "xs:long",
        "required": false
      },
      {
        "name": "NEXTID",
        "type": "xs:long",
        "required": false
      },
      {
        "name": "UPDATEDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "STARTDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "ENDDATE",
        "type": "xs:date",
        "required": true
      },
      {
        "name": "ISACTUAL",
        "type": "xs:boolean",
        "required": true
      },
      {
        "name": "ISACTIVE",
        "type": "xs:boolean",
        "required": true
      }
    ]
  },
  "AS_CHANGE_HISTORY": {
    "path": "AS_CHANGE_HISTORY_251_21_04_01_01.xsd",
    "name": "AS_CHANGE_HISTORY",
    "part": "251_21",
    "format": "04_01_01",
    "list": "ITEMS",
    "item": "ITEM",
    "fields": [
      {
        "name": "CHANGEID",
        "type": "xs:long",
        "required": true
      },
      {
        "name": "OBJECTID",
        "type": "xs:long",
        "required": true
      },
      {
        "name": "ADROBJECTID",
        "type": "xs:string",
        "required": true,
        "length": "36"
      },
      {
        "name": "OPERTYPEID",
        "type": "xs:integer",
        "required": true
      },
      {
        "name": "NDOCID",
        "type": "xs:long",
        "required": false
      },
      {
        "name": "CHANGEDATE",
        "type": "xs:date",
        "required": true
      }
    ]
  }
}

export default types