import type { Readable } from "stream"
import type { Pool } from "pg"
import type { File } from "unzipper"
import type { DownloadFileInfo } from "./nalog"

import path from "path"
import fs from "fs"
import { pipeline } from "stream/promises"
import { stringify as csvstream } from "csv-stringify"
import { tables, createTable } from "./gar"
import { GARObjectStream } from "./GARObjectStream"
import { fileMetaFromUrl, zipFromUrl, pgCopyFrom, retry } from "./utils"
import * as q from "./db/queries"

export async function load(
  pg: Pool,
  info: DownloadFileInfo,
  garUrl: string,
  prefixes: Array<string>,
  regions: null | Array<string> = null,
  cache = false,
  nextPrefix = "next"
) {
  const meta = await fileMetaFromUrl(garUrl)

  const versionId = await q.insertLogVersion(
    pg,
    info.VersionId,
    info.TextVersion,
    info.Date,
    garUrl,
    meta.lastModified,
    meta.contentLength
  )
  const loadedFiles = await q.selectLoadedFiles(
    pg,
    info.VersionId,
    garUrl,
    meta.contentLength
  )

  if (loadedFiles.length === 0) {
    await cleanup(pg, nextPrefix, prefixes)
  }

  const zip = await zipFromUrl(garUrl, meta.contentLength)
  const files = zip.files.filter((file) => {
    if (loadedFiles.includes(file.path)) return false

    const region = file.path[2] === "/" ? file.path.substring(0, 2) : null
    if (regions !== null && region !== null && regions.indexOf(region) === -1)
      return false

    const prefix = file.path.substring(
      region === null ? 0 : 3,
      file.path.length - 50
    )
    return prefixes.includes(prefix)
  })
  const loadingState = new LoadingState(
    files.reduce((s, f) => s + f.uncompressedSize, 0)
  )

  for (const file of files) {
    const prefix = file.path.substring(
      file.path[2] === "/" ? 3 : 0,
      file.path.length - 50
    )

    const table = tables.find((t) => t.prefix === prefix)
    if (table === undefined) throw new Error("Unexpected prefix " + prefix)

    const nextTable = [nextPrefix, prefix.toLowerCase()].join("_")
    await createTable(pg, nextTable, table.type)

    const startDate = new Date()

    // min file size: BOM (3) + xml declaration (38)
    //                + root tag open (type.list + 2)
    //                + root tag close (type.list + 3)
    const minSize = 46 + table.type.list.length * 2
    if (file.uncompressedSize > minSize) {
      const sizeStr = (file.uncompressedSize / 1024 / 1024).toFixed(3)
      const columns = table.type.fields.map((f) => f.name)
      await retry(3, async () => {
        const dt = new Date().toISOString()
        console.log(`${dt} ${file.path} loading ${sizeStr}mb, ${loadingState}`)

        return pipeline(
          await readFile(file, cache),
          new GARObjectStream(table.type),
          csvstream({ columns, quoted_string: true }),
          await pgCopyFrom(pg, `copy ${nextTable} from stdin csv`)
        )
      })
    } else {
      const dt = startDate.toISOString()
      console.log(`${dt} ${file.path} skiping <${minSize}b, ${loadingState}`)
    }
    await q.insertLogFile(pg, versionId, file.path, file.uncompressedSize)

    loadingState.loaded += file.uncompressedSize
    loadingState.elapsed += Date.now() - startDate.getTime()
  }
  console.log(`${new Date().toISOString()} ${loadingState}`)

  return versionId
}

export async function cleanup(
  pg: Pool,
  tablePrefix: string,
  prefixes: Array<string>
): Promise<void> {
  for (const prefix of prefixes.slice().reverse()) {
    const table = [tablePrefix, prefix.toLowerCase()].join("_")
    await pg.query(`drop table if exists ${table}`)
  }
  await q.deleteLoadedFiles(pg)
}

async function readFile(file: File, cache: boolean): Promise<Readable> {
  if (cache === false) return file.stream().setEncoding("utf8")

  const cachePath = path.join(__dirname, "..", "data")
  const filePath = path.join(cachePath, file.path)
  const stat = await fs.promises.stat(filePath).catch(() => ({ size: 0 }))
  if (stat.size !== file.uncompressedSize) {
    await fs.promises
      .mkdir(path.dirname(filePath), { recursive: true })
      .catch((error) => error)
    await pipeline(file.stream(), fs.createWriteStream(filePath))
  }
  return fs.createReadStream(filePath, { encoding: "utf8" })
}

class LoadingState {
  constructor(
    public size: number,
    public loaded: number = 0,
    public elapsed: number = 0
  ) {}

  toString() {
    const { size, loaded, elapsed } = this
    const sizeStr = (size / 1024 / 1024).toFixed(3)

    if (loaded === 0 || elapsed === 0) return `loaded 0/${sizeStr}mb`

    const elasedStr = (elapsed / 1000).toFixed(3)
    if (size === loaded) return `loaded ${sizeStr}mb in ${elasedStr}s`

    const loadedStr = (loaded / 1024 / 1024).toFixed(3)
    const estimated = (size - loaded) / (loaded / elapsed)
    const estimatedStr = new Date(Date.now() + estimated).toISOString()
    return `loaded ${loadedStr}/${sizeStr}mb, estimated end ${estimatedStr}`
  }
}
