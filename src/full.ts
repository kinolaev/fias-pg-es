import type { Pool } from "pg"
import type { Client as ESClient } from "./elastic"

import * as q from "./db/queries"
import { getDownloadFileInfo, getLastDownloadFileInfo } from "./nalog"
import * as config from "./config"
import { pgTransaction, esCopyFromPg } from "./utils"
import { tables } from "./gar"
import { load } from "./load"

export async function full(
  pg: Pool,
  es: ESClient,
  version: null | number,
  prefixes: Array<string>,
  regions: null | Array<string> = null,
  cache: boolean = false
): Promise<void> {
  const nextPrefix = "next"
  const prevPrefix = "prev"

  const info =
    version === null
      ? await getLastDownloadFileInfo()
      : await getDownloadFileInfo(version)

  await cleanup(pg, prevPrefix, prefixes)

  const versionId = await load(
    pg,
    info,
    info.GarXMLFullURL,
    prefixes,
    regions,
    cache,
    nextPrefix
  )

  for (const prefix of prefixes) {
    const nextTable = [nextPrefix, prefix.toLowerCase()].join("_")
    const table = tables.find((t) => t.prefix === prefix)
    if (table === undefined) throw new Error("Unexpected prefix " + prefix)

    console.log(
      `${new Date().toISOString()} ${nextTable} adding primary key ${table.key}`
    )
    await pg.query(`alter table ${nextTable} add primary key (${table.key})`)
    for (const foreign of table.foreign) {
      const refTable = prefixes.includes(foreign.ref.table)
        ? [nextPrefix, foreign.ref.table].join("_")
        : foreign.ref.table
      console.log(
        `${new Date().toISOString()} ${nextTable} adding foreign key ${
          foreign.key
        }`
      )
      await pg.query(`
        alter table ${nextTable} add constraint ${nextTable}_${foreign.key}_fkey
        foreign key (${foreign.key}) references ${refTable} (${foreign.ref.key})
      `)
    }
    for (const key of table.indices) {
      console.log(
        `${new Date().toISOString()} ${nextTable} adding index ${key}`
      )
      await pg.query(
        `create index ${nextTable}_${key} on ${nextTable} (${key})`
      )
    }
  }

  await pgTransaction(pg, async (client) => {
    for (const prefix of prefixes) {
      const mainTable = prefix.toLowerCase()
      const prevTable = [prevPrefix, mainTable].join("_")
      const nextTable = [nextPrefix, mainTable].join("_")
      const table = tables.find((t) => t.prefix === prefix)
      if (table === undefined) throw new Error("Unexpected prefix " + prefix)

      console.log(
        `${new Date().toISOString()} ${nextTable} renaming to ${mainTable}`
      )

      await client.query(
        `alter index if exists ${mainTable}_pkey rename to ${prevTable}_pkey`
      )
      await client.query(
        `alter index ${nextTable}_pkey rename to ${mainTable}_pkey`
      )

      for (const foreign of table.foreign) {
        await client.query(
          `alter index if exists ${mainTable}_${foreign.key}_fkey rename to ${prevTable}_${foreign.key}_fkey`
        )
        await client.query(
          `alter index ${nextTable}_${foreign.key}_fkey rename to ${mainTable}_${foreign.key}_fkey`
        )
      }

      for (const key of table.indices) {
        await client.query(
          `alter index if exists ${mainTable}_${key} rename to ${prevTable}_${key}`
        )
        await client.query(
          `alter index ${nextTable}_${key} rename to ${mainTable}_${key}`
        )
      }

      await client.query(
        `alter table if exists ${mainTable} rename to ${prevTable}`
      )
      await client.query(`alter table ${nextTable} rename to ${mainTable}`)
    }

    for (const hierarchy of config.hierarchies) {
      await client.query(
        `alter view if exists as_${hierarchy}_houses_index rename to ${prevPrefix}_as_${hierarchy}_houses_index`
      )
      await client.query(
        `alter view if exists as_${hierarchy}_addr_obj_index rename to ${prevPrefix}_as_${hierarchy}_addr_obj_index`
      )
      await client.query(
        `alter materialized view if exists as_${hierarchy}_addr_obj_fullname rename to ${prevPrefix}_as_${hierarchy}_addr_obj_fullname`
      )
    }

    await q.deleteLoadedFiles(client)
    await q.versionLoaded(client, versionId)
  })

  await cleanup(pg, prevPrefix, prefixes)

  await fullIndex(pg, es)
}

export async function cleanup(
  pg: Pool,
  tablePrefix: string,
  prefixes: Array<string>
): Promise<void> {
  for (const hierarchy of config.hierarchies) {
    await pg.query(`drop view if exists ${tablePrefix}_as_${hierarchy}_houses_index`)
    await pg.query(
      `drop view if exists ${tablePrefix}_as_${hierarchy}_addr_obj_index`
    )
    await pg.query(
      `drop materialized view if exists ${tablePrefix}_as_${hierarchy}_addr_obj_fullname`
    )
  }
  for (const prefix of prefixes.slice().reverse()) {
    const table = [tablePrefix, prefix.toLowerCase()].join("_")
    await pg.query(`drop table if exists ${table}`)
  }
}

export async function fullIndex(pg: Pool, es: ESClient) {
  const indexPostfix = Date.now().toString()

  for (const hierarchy of config.hierarchies) {
    const prefix = "AS_ADDR_OBJ"
    const table = tables.find((t) => t.prefix === prefix)
    if (table === undefined) throw new Error("Unexpected prefix " + prefix)

    await pg.query(`drop view if exists as_${hierarchy}_houses_index`)
    await pg.query(`drop view if exists as_${hierarchy}_addr_obj_index`)
    await pg.query(
      `drop materialized view if exists as_${hierarchy}_addr_obj_fullname`
    )

    console.log(
      `${new Date().toISOString()} as_${hierarchy}_addr_obj_fullname making fullnames`
    )
    await pg.query(
      `create materialized view as_${hierarchy}_addr_obj_fullname as select id, objectid, objectguid, level, get_addr_obj_fullname(objectid, '${hierarchy}') fullname from as_addr_obj where isactual = true and isactive = true`
    )
    await pg.query(
      `create index as_${hierarchy}_addr_obj_fullname_objectid on as_${hierarchy}_addr_obj_fullname (objectid)`
    )
    await pg.query(
      `create view as_${hierarchy}_addr_obj_index as select concat('a_', objectguid) id, level, fullname from as_${hierarchy}_addr_obj_fullname where fullname is not null`
    )
    await pg.query(
      `create view as_${hierarchy}_houses_index as select concat('h_', h.objectguid) id, 32767 as level, concat(a.fullname, ' ', get_house_postfix(h.id)) fullname from as_houses h, as_adm_hierarchy p, as_${hierarchy}_addr_obj_fullname a where h.isactual = true and h.isactive = true and p.isactive = true and h.objectid = p.objectid and p.parentobjid = a.objectid and a.fullname is not null`
    )

    const alias = `gar_${hierarchy}`
    const index = [alias, indexPostfix].join("_")
    console.log(
      `${new Date().toISOString()} as_${hierarchy}_addr_obj_index indexing`
    )
    await esCopyFromPg(pg, es, `as_${hierarchy}_addr_obj_index`, "index", index)
    console.log(
      `${new Date().toISOString()} as_${hierarchy}_houses_index indexing`
    )
    await esCopyFromPg(pg, es, `as_${hierarchy}_houses_index`, "index", index)
    await es.refresh(index)

    const indices = Object.keys(await es.alias(alias).catch(() => ({})))
    const deletingLog =
      indices.length === 0 ? "" : ` and deleting ${indices.join(", ")}`
    console.log(
      `${new Date().toISOString()} ${alias} setting ${index}${deletingLog}`
    )
    await es.aliases([
      { add: { index, alias } },
      ...indices.map((index) => ({ remove_index: { index } })),
    ])
  }
}
