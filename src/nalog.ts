import { a1, new_ } from "elow"
import * as Json from "elow/lib/Json/Decode"
import { httpRequest, readBufferJsonStream, zipFromUrl } from "./utils"

// https://fias.nalog.ru/Updates

export const garSchemasUrl = "https://fias.nalog.ru/docs/gar_schemas.zip"
export const allUrl =
  "https://fias.nalog.ru/WebServices/Public/GetAllDownloadFileInfo"
export const lastUrl =
  "https://fias.nalog.ru/WebServices/Public/GetLastDownloadFileInfo"

export const getGARSchemas = () => zipFromUrl(garSchemasUrl)

export class DownloadFileInfo {
  constructor(
    public VersionId: number,
    public TextVersion: string,
    public FiasCompleteDbfUrl: string,
    public FiasDeltaDbfUrl: string,
    public GarXMLFullURL: string,
    public GarXMLDeltaURL: string,
    public Date: string
  ) {}
}

export const getAllDownloadFileInfo = (): Promise<Array<DownloadFileInfo>> =>
  httpRequest(allUrl, {})
    .then(readBufferJsonStream)
    .then(downloadFileInfoListDecoder)
    .then(unwrapDecoded)

export const getLastDownloadFileInfo = (): Promise<DownloadFileInfo> =>
  httpRequest(lastUrl, {})
    .then(readBufferJsonStream)
    .then(downloadFileInfoDecoder)
    .then(unwrapDecoded)

export const getDownloadFileInfo = (
  versionId: number
): Promise<DownloadFileInfo> =>
  getAllDownloadFileInfo().then((infos) => {
    const info = infos.find((info) => info.VersionId === versionId)
    if (info === undefined) {
      throw new Error(`Version ${versionId.toString()} not found`)
    }
    return info
  })

const downloadFileInfoDecoder = Json.map(
  a1(new_, DownloadFileInfo),
  Json.field("VersionId", Json.number),
  Json.field("TextVersion", Json.string),
  Json.field("FiasCompleteDbfUrl", Json.string),
  Json.field("FiasDeltaDbfUrl", Json.string),
  Json.field("GarXMLFullURL", Json.string),
  Json.field("GarXMLDeltaURL", Json.string),
  Json.field("Date", Json.string)
)

const downloadFileInfoListDecoder = Json.list(downloadFileInfoDecoder)

function unwrapDecoded<T>(decoded: Json.Decoded<T>): T {
  if (decoded.type === "Ok") return decoded.data[0]
  throw new Error(Json.decodeErrorToString(decoded.data[0]))
}
