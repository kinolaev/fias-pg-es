import path from "path"
import { ChildProcess, fork } from "child_process"
import http from "http"

class State {
  constructor(
    public data: {
      command?: string
      startAt?: string
      endAt?: string
      code?: number | null
      reason?: NodeJS.Signals | null
    },
    public child: ChildProcess | null,
    public loopTimeout: number = 0,
    public nextLoopTimeout: number = 0
  ) {}
}

function exec(args: Array<string>): Promise<void> {
  if (state.child !== null) return Promise.resolve()

  return new Promise((resolve) => {
    state.data = { command: args.join(" "), startAt: new Date().toISOString() }
    state.child = fork(path.join(__dirname, "cli.js"), args, {
      cwd: process.cwd(),
      env: process.env,
      stdio: "inherit",
    }).on("exit", (code, reason) => {
      state.data.endAt = new Date().toISOString()
      state.data.code = code
      state.data.reason = reason
      state.child = null
      console.log(state.data.endAt, "exit", code, reason)
      resolve()
    })
    console.log(state.data.startAt, "exec", state.data.command)
  })
}

async function loop(args: Array<string>, timeout: number): Promise<void> {
  state.nextLoopTimeout = timeout * 1000
  if (isFinite(state.loopTimeout) && state.loopTimeout > 0) return
  
  state.loopTimeout = state.nextLoopTimeout
  console.log(new Date().toISOString(), "loop start")
  while (isFinite(state.loopTimeout) && state.loopTimeout > 0) {
    await exec(args)
    await new Promise((resolve) => setTimeout(resolve, state.nextLoopTimeout))
    state.loopTimeout = state.nextLoopTimeout
  }
  console.log(new Date().toISOString(), "loop end")
}

const state = new State({}, null)

const srv = http.createServer((req, res) => {
  if (req.headers["user-agent"]?.startsWith("kube-probe/")) return res.end()

  const url = new URL(req.url ?? "/", "http://localhost")

  if (url.pathname === "/abort") {
    if (state.child?.killed === false) {
      state.child.kill()
    }
  } else if (url.pathname === "/loop") {
    loop(["delta"], +(url.searchParams.get("timeout") ?? "0"))
  } else {
    const args = [url.pathname.substring(1)]
    for (const [k, v] of url.searchParams) {
      args.push("-" + k)
      if (v !== "") args.push(v)
    }
    exec(args)
  }

  const body = JSON.stringify(state.data)
  res.writeHead(200, {
    "content-type": "application/json",
    "content-length": Buffer.byteLength(body).toString(),
  })
  res.end(body)
})

srv.listen(process.env.HTTP_PORT ?? 5000, () => {
  console.log("listening at", srv.address())
})

loop(["delta"], +(process.env.LOOP_TIMEOUT ?? "0"))
