import { pool } from "./pool"
import { client } from "./es"
import { full, fullIndex } from "./full"
import { generate } from "./generate"
import { delta } from "./delta"
import * as config from "./config"

async function main(...args: Array<string>): Promise<void> {
  let command = ""
  let version = null
  let prefixes = config.prefixes
  let regions
  let cache

  let key = null
  for (const arg of args) {
    if (key === null) {
      if (arg === "-c") {
        cache = true
      } else if (arg[0] === "-") {
        key = arg
      } else {
        command = arg
      }
    } else {
      if (key === "-v") {
        version = +arg
      } else if (key === "-p") {
        prefixes = arg.split(",")
      } else if (key === "-r") {
        regions = arg.split(",")
      }
      key = null
    }
  }

  switch (command) {
    case "generate":
      return generate()
    case "full":
      return full(pool, client, version, prefixes, regions, cache)
    case "full-index":
      return fullIndex(pool, client)
    case "delta":
      return delta(pool, client, version, prefixes, regions, cache)
    default:
      throw new Error("Unknown command")
  }
}

main(...process.argv.slice(2))
  .catch(console.error)
  .then(() => pool.end())
