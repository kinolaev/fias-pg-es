import type { TransformOptions } from "stream"
import type { IncomingMessage } from "http"
import type { Pool, PoolClient } from "pg"
import type { CentralDirectory } from "unzipper"
import type { Client as ESClient, BulkAction } from "./elastic"

import { Readable } from "stream"
import { pipeline } from "stream/promises"
import http from "http"
import https from "https"
import ProxyAgent from "proxy-agent"
import pgCopy from "pg-copy-streams"
import QueryStream from "pg-query-stream"
import zip from "./zip"

export function httpRequest(
  url: string | URL,
  options: https.RequestOptions,
  body?: string | Buffer
): Promise<IncomingMessage> {
  return new Promise((resolve, reject) => {
    // prettier-ignore
    const mod = url instanceof URL
      ? url.protocol === "http:" ? http : https
      : url.startsWith("http:") ? http : https
    const agent = new ProxyAgent()
    const timeout = 60000
    const req = mod.request(url, { agent, timeout, ...options }, resolve)
    req.on("timeout", () => {
      req.destroy()
    })
    req.on("error", reject)
    req.end(body)
  })
}

export class HttpsRequestStream extends Readable {
  _res: IncomingMessage | null = null

  constructor(
    url: string | URL,
    options: https.RequestOptions,
    body?: string | Buffer
  ) {
    super()

    httpRequest(url, options, body)
      .then((res) => {
        if (
          res.statusCode === undefined ||
          res.statusCode < 200 ||
          res.statusCode > 299
        ) {
          console.error(
            new Date().toISOString(),
            "HttpsRequestStream res",
            res.statusCode,
            res.headers
          )
          res.destroy()
          this.destroy(new Error("InvalidStatusCode"))
          return
        }

        this._res = res

        res.on("error", (error) => {
          console.error(
            new Date().toISOString(),
            "HttpsRequestStream err",
            res.statusCode,
            res.headers,
            error
          )
          this._res = null
          this.destroy(error)
        })
        res.on("data", (chunk) => {
          if (this.push(chunk) === false) {
            res.pause()
          }
        })
        res.on("end", () => {
          this._res = null
          this.push(null)
        })
      })
      .catch((error) => {
        console.error(new Date().toISOString(), "HttpsRequestStream req", error)
        this.destroy(error)
      })
  }
  _read() {
    if (this._res?.isPaused()) this._res.resume()
  }
  _destroy(
    error: Error | null,
    callback: (error?: Error | null | undefined) => void
  ): void {
    if (this._res) {
      this._res.destroy(error ?? undefined)
      this._res = null
    }
    callback(error)
  }
}

interface FileMeta {
  lastModified: Date
  contentLength: number
}

function fileMetaFromIncomingMessage(res: IncomingMessage): FileMeta {
  const lastModifiedHeader = res.headers["last-modified"]
  if (lastModifiedHeader === undefined) {
    throw new Error("No last-modified header")
  }
  const lastModified = new Date(lastModifiedHeader)
  if (Number.isNaN(lastModified.getTime())) {
    throw new Error("Invalid last-modified header: " + lastModifiedHeader)
  }

  const contentLengthHeader = res.headers["content-length"]
  if (contentLengthHeader === undefined) {
    throw new Error("No content-length header")
  }
  const contentLength = +contentLengthHeader
  if (contentLength.toString() !== contentLengthHeader) {
    throw new Error("Invalid content-length header: " + contentLengthHeader)
  }

  return { lastModified, contentLength }
}

export function fileMetaFromUrl(url: string | URL): Promise<FileMeta> {
  return httpRequest(url, { method: "head" }).then(fileMetaFromIncomingMessage)
}

export function readSteam<T>(stream: Readable): Promise<Array<T>> {
  return new Promise((resolve, reject) => {
    const chunks: Array<T> = []
    stream.on("error", reject)
    stream.on("data", (chunk) => {
      chunks.push(chunk)
    })
    stream.on("end", () => {
      resolve(chunks)
    })
  })
}

export function readBufferStream(stream: Readable): Promise<Buffer> {
  return readSteam<Buffer>(stream).then(Buffer.concat)
}

export function readBufferStringStream(
  stream: Readable,
  encoding?: BufferEncoding
): Promise<string> {
  return readBufferStream(stream).then((buf) => buf.toString(encoding))
}

export function readBufferJsonStream(stream: Readable): Promise<any> {
  return readBufferStringStream(stream).then(JSON.parse)
}

export function zipFromUrl(
  url: string | URL,
  size?: number
): Promise<CentralDirectory> {
  return zip({
    stream(offset, length) {
      const end = length ? (offset + length).toString() : ""
      const headers = { range: `bytes=${offset}-${end}` }
      return new HttpsRequestStream(url, { headers })
    },
    size() {
      return size
        ? Promise.resolve(size)
        : fileMetaFromUrl(url).then((meta) => meta.contentLength)
    },
  })
}

export async function pgTransaction<T>(
  pool: Pool,
  execute: (client: PoolClient) => Promise<T>
): Promise<T> {
  const client = await pool.connect()
  try {
    await client.query("begin")
    const result = await execute(client)
    await client.query("commit")
    return result
  } catch (error) {
    await client.query("rollback")
    throw error
  } finally {
    client.release()
  }
}

export async function pgCopyFrom(
  pg: Pool,
  txt: string,
  options?: TransformOptions
): Promise<pgCopy.CopyStreamQuery> {
  const client = await pg.connect()
  return client
    .query(pgCopy.from(txt, options))
    .on("error", (error) => {
      client.release()
    })
    .on("finish", () => {
      client.release()
    })
}

export async function esCopyFromPg(
  pg: Pool,
  es: ESClient,
  tableName: string,
  action: BulkAction,
  index: string,
  type: string = "data"
): Promise<void> {
  const client = await pg.connect()

  return pipeline(
    client.query(new QueryStream(`select * from ${tableName}`)),
    es.bulk(action, index, type)
  ).finally(() => {
    client.release()
  })
}

export function retry<T>(count: number, fn: () => Promise<T>): Promise<T> {
  return count > 1 ? fn().catch(() => retry(count - 1, fn)) : fn()
}
