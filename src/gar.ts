import type { Pool } from "pg"

import xml2js from "xml2js"

export class GARType {
  constructor(
    public path: string,
    public name: string,
    public part: string,
    public format: string,
    public list: string,
    public item: string,
    public fields: Array<GARField>
  ) {}
}

export class GARField {
  constructor(
    public name: string,
    public type: string,
    public required: boolean,
    public length?: string,
    public maxLength?: string
  ) {}
}

export { default as tables } from "./gar.tables"

export async function createTable(pool: Pool, table: string, type: GARType) {
  const columnsSql = type.fields.map(columnDefinitionFromField).join(",\n")
  const tableSql = `create table if not exists ${table} (\n${columnsSql}\n)`
  await pool.query(tableSql).catch((error) => {
    console.log(tableSql)
    throw error
  })
}

export function columnDefinitionFromField(field: GARField): string {
  const fieldName = field.name.toLowerCase()
  const fieldType = fieldTypes[field.type.substring(3)]!(field)
  const notNull = field.required ? " not null" : ""
  return `  "${fieldName}" ${fieldType}${notNull}`
}

export async function typeFromXsd(
  filePath: string,
  buf: Buffer
): Promise<GARType> {
  const xml = await xml2js.parseStringPromise(buf)
  const root = xml["xs:schema"]["xs:element"]
  const list = root[0]
  let item = list["xs:complexType"][0]["xs:sequence"][0]["xs:element"][0]
  if (item.$.ref) {
    item = root.find((n: any) => n.$.name === item.$.ref)
  }
  const attributes = item["xs:complexType"][0]["xs:attribute"] as Array<any>

  let nsIndex = filePath.indexOf("_2_251_")
  let partIndex = nsIndex + "_2_251_".length
  if (nsIndex === -1) {
    nsIndex = filePath.indexOf("_251_")
    partIndex = nsIndex + "_251_".length
  }
  const formatIndex = filePath.indexOf("_", partIndex)
  const extIndex = filePath.indexOf(".xsd", formatIndex)

  const prefix = filePath.substring(0, nsIndex)
  const garType = new GARType(
    filePath,
    filePath.substring(0, nsIndex),
    "251_" + filePath.substring(partIndex, formatIndex),
    filePath.substring(formatIndex + 1, extIndex),
    list.$.name,
    item.$.name,
    attributes.map(fieldFromNode)
  )

  // fix gar schema errors
  if (prefix === "AS_ADDR_OBJ") {
    const fields = garType.fields
    const levelIndex = fields.findIndex((f) => f.name === "LEVEL")
    if (levelIndex !== -1) {
      fields[levelIndex] = { name: "LEVEL", type: "xs:integer", required: true }
    }
  }

  return garType
}

export function fieldFromNode(node: any): GARField {
  const field = new GARField(
    node.$.name,
    node.$.type,
    node.$.use === "required"
  )

  const restriction = node["xs:simpleType"]?.[0]["xs:restriction"][0]
  if (restriction === undefined) return field

  field.type = restriction.$.base
  if (field.type === "xs:string") {
    const length = restriction["xs:length"]?.[0].$.value
    if (length) {
      field.length = length
    }
    const maxLength = restriction["xs:maxLength"]?.[0].$.value
    if (maxLength) {
      field.maxLength = maxLength
    }
  } else if (field.type === "xs:integer") {
    const enumeration = restriction["xs:enumeration"] ?? []
    if (
      enumeration.length === 2 &&
      enumeration[0].$.value === "0" &&
      enumeration[1].$.value === "1"
    ) {
      field.type = "xs:boolean"
    }
  }

  return field
}

const fieldTypes: { [name: string]: (field: GARField) => string } = {
  boolean: (f) => "boolean",
  integer: (f) => "int",
  long: (f) => "bigint",
  string: (f) =>
    f.length
      ? `char(${f.length})`
      : f.maxLength
      ? `varchar(${f.maxLength})`
      : "text",
  date: (f) => "date",
}
