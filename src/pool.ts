import { Pool } from "pg"

const defaultConnectionString =
  "postgres://postgres:postgres@postgres:5432/postgres?sslmode=disable"

export const pool = new Pool({
  connectionString: process.env.DATABASE_URL ?? defaultConnectionString,
})
