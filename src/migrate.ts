import path from "path"
import fs from "fs/promises"
import { pool as pg } from "./pool"
import { pgTransaction } from "./utils"

async function main(): Promise<void> {
  const migrationsPath = path.join(__dirname, "..", "migrations")
  const files = await fs.readdir(migrationsPath)
  for (const file of files) {
    if (file.endsWith(".sql") === false) continue
    const sql = await fs.readFile(path.join(migrationsPath, file), "utf8")
    console.log(new Date().toISOString(), "executing", file)
    await pgTransaction(pg, async (client) => {
      return client.query(sql)
    })
  }
  console.log(new Date().toISOString(), "done")
}

main()
  .catch(console.error)
  .then(() => pg.end())
