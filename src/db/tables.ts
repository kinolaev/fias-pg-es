export interface LogVersion {
  id: number
  version_id: number
  text_version: string
  date: string
  url: string
  last_modified: Date
  content_length: number
  created_at: Date
  loaded_at: Date
}

export interface LogFile {
  log_version_id: number
  path: string
  size: number
  created_at: Date
}

export interface Lock {
  id: number
  created_at: Date
}
