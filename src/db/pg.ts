import type { Pool as PGPool } from "pg"

export type Pool = Pick<PGPool, "query">
