import type { Pool } from "pg"
import type { LogFile } from "./tables"

const queryText = `insert into log_files(log_version_id, path, size) values($1, $2, $3)`

type Values = [
  log_version_id: LogFile["log_version_id"],
  path: LogFile["path"],
  size: LogFile["size"]
]

type Row = {}

export const query = (pool: Pool, ...v: Values): Promise<number> =>
  pool.query<Row, Values>(queryText, v).then((result) => result.rowCount)
