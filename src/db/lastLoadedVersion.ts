import type { Pool } from "pg"
import type { LogVersion } from "./tables"

const queryText = `
  select version_id
  from log_versions
  where loaded_at is not null
  order by created_at desc
  limit 1
`

type Values = []

type Row = Pick<LogVersion, "version_id">

export const query = (pool: Pool): Promise<Row> =>
  pool.query<Row, Values>(queryText).then((result) => {
    if (result.rows[0]) return result.rows[0]
    throw new Error("Last version not found")
  })
