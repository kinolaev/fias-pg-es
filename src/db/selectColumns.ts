import type { Pool } from "pg"

const queryText = `
  select column_name
  from information_schema.columns
  where table_name = $1
  order by ordinal_position
`

type Values = [tableName: string]

type Row = { column_name: string }

export const query = (pool: Pool, ...v: Values): Promise<Array<string>> =>
  pool
    .query<Row, Values>(queryText, v)
    .then((result) => result.rows.map((row) => row.column_name))
