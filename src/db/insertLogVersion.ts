import type { Pool } from "pg"
import type { LogVersion } from "./tables"

const queryText = `
  insert into log_versions(version_id, text_version, date, url, last_modified, content_length)
  values($1, $2, $3, $4, $5, $6)
  returning id
`

type Values = [
  version_id: LogVersion["version_id"],
  text_version: LogVersion["text_version"],
  date: LogVersion["date"],
  url: LogVersion["url"],
  last_modified: LogVersion["last_modified"],
  content_length: LogVersion["content_length"]
]

type Row = Pick<LogVersion, "id">

export const query = (pool: Pool, ...v: Values): Promise<number> =>
  pool.query<Row, Values>(queryText, v).then((result) => result.rows[0]!.id)
