import type { Pool, QueryResult } from "pg"

const queryText = "delete from lock where id = 1"

export const query = (pool: Pool): Promise<QueryResult<{}>> =>
  pool.query(queryText)
