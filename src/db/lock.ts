import type { Pool } from "pg"

const queryText = `
  insert into lock(id) values(1)
  on conflict(id) do
    update set created_at = excluded.created_at
    where extract(epoch from now() - lock.created_at) > $1
`

export const query = (pool: Pool, timeout: number): Promise<void> =>
  pool.query(queryText, [timeout]).then((result) => {
    if (result.rowCount === 1) return
    throw new Error("Can't insert lock")
  })
