import type { Pool } from "./pg"
import type { LogVersion } from "./tables"

const queryText = `update log_versions set loaded_at = now() where id = $1`

type Values = [id: LogVersion["id"]]

type Row = {}

export const query = (pool: Pool, ...v: Values): Promise<number> =>
  pool.query<Row, Values>(queryText, v).then((result) => result.rowCount)
