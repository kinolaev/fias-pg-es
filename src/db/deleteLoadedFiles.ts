import type { Pool } from "./pg"

const queryText = `delete from log_files`

type Values = []

type Row = {}

export const query = (pool: Pool, ...v: Values): Promise<number> =>
  pool.query<Row, Values>(queryText, v).then((result) => result.rowCount)
