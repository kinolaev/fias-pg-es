import type { Pool } from "pg"
import type { LogFile, LogVersion } from "./tables"

const queryText = `
  select path from log_files
  where
    log_version_id in (select id from log_versions where version_id = $1 and url = $2 and content_length = $3)
`

type Values = [
  version_id: LogVersion["version_id"],
  url: LogVersion["url"],
  content_length: LogVersion["content_length"]
]

type Row = Pick<LogFile, "path">

export const query = (pool: Pool, ...v: Values): Promise<Array<string>> =>
  pool
    .query<Row, Values>(queryText, v)
    .then((result) => result.rows.map((row) => row.path))
