import { Writable } from "stream"
import http from "http"
import https from "https"
import {
  httpRequest,
  readBufferJsonStream,
  readBufferStringStream,
} from "./utils"

export class Client {
  constructor(private url: string) {}

  async alias(alias: string): Promise<AliasResponse> {
    return this.request("/_alias/" + alias, { method: "GET" })
  }

  async aliases(actions: Array<AliasAction>): Promise<AliasesResponse> {
    const body = JSON.stringify({ actions })
    const headers = {
      "content-type": "application/json",
      "content-length": Buffer.byteLength(body).toString(),
    }
    return this.request("/_aliases", { method: "POST", headers }, body)
  }

  bulk(
    action: BulkAction,
    index: string,
    type: string = "_doc",
    id: string = "id"
  ): Writable {
    return new BulkStream(this.url + `/${index}/${type}/_bulk`, action, id)
  }

  refresh(index: string): Promise<void> {
    return this.request(`/${index}/_refresh`, { method: "POST" })
  }

  private async request(
    url: string,
    options: https.RequestOptions,
    body?: string
  ): Promise<any> {
    const res = await httpRequest(this.url + url, options, body)
    const statusCode = res.statusCode
    if (statusCode === undefined) {
      throw new Error("Status code is undefined")
    }
    if (200 <= statusCode && statusCode < 300) {
      return readBufferJsonStream(res)
    }
    const msg = await readBufferStringStream(res)
    throw new Error(msg || "Status " + statusCode.toString())
  }
}

export type BulkAction = "index" | "create" | "delete"

interface AliasResponse {
  [index: string]: { aliases: { [alias: string]: {} } }
}

type AliasAction =
  | { add: { index: string; alias: string } }
  | { remove_index: { index: string } }

interface AliasesResponse {
  acknowledged: boolean
}

class BulkStream extends Writable {
  _bulkChunks: Array<string>
  _bulkOffset = 0
  _bulkCount = 0
  _bulkStart = Date.now()

  constructor(
    private _bulkUrl: string,
    private _bulkAction: string,
    private _bulkId: string,
    limit = 100000
  ) {
    super({ objectMode: true })
    const length = _bulkAction === "delete" ? limit : limit * 2
    this._bulkChunks = new Array(length + 1)
  }

  _write(
    chunk: any,
    encoding: BufferEncoding,
    callback: (err?: Error | null | undefined) => void
  ): void {
    const _id = chunk[this._bulkId]
    this._bulkChunks[this._bulkOffset++] = JSON.stringify({
      [this._bulkAction]: { _id },
    })
    if (this._bulkAction !== "delete") {
      chunk[this._bulkId] = undefined
      this._bulkChunks[this._bulkOffset++] = JSON.stringify(chunk)
      chunk[this._bulkId] = _id
    }
    this._bulkCount++
    if (this._bulkOffset + 1 < this._bulkChunks.length) {
      callback()
    } else {
      this._final(callback)
    }
  }

  _final(callback: (err?: Error | null | undefined) => void): void {
    if (this._bulkOffset === 0) return callback()
    this._bulkChunks[this._bulkOffset++] = ""
    if (this._bulkOffset < this._bulkChunks.length) {
      this._bulkChunks.length = this._bulkOffset
    }
    this._bulkOffset = 0

    const body = this._bulkChunks.join("\n")
    const headers = {
      "content-type": "application/x-ndjson",
      "content-length": Buffer.byteLength(body),
    }
    const mod = this._bulkUrl.startsWith("http:") ? http : https
    const req = mod.request(
      this._bulkUrl,
      { method: "POST", headers },
      (res) => {
        if (res.statusCode === undefined) {
          callback(new Error("Status code is undefined"))
        } else if (200 <= res.statusCode && res.statusCode < 300) {
          res.on("error", callback)
          res.on("data", (chunk) => {})
          res.on("end", () => {
            const end = new Date()
            const spent = (end.getTime() - this._bulkStart) / 1000
            console.log(end.toISOString(), "es.bulk", this._bulkCount, spent)
            callback()
          })
        } else {
          readBufferStringStream(res)
            .then((msg) =>
              callback(new Error(msg || `Status ${res.statusCode}`))
            )
            .catch(callback)
        }
      }
    )
    req.on("error", callback)
    req.end(body)
  }
}
