FROM node:18-alpine AS build

WORKDIR /app

COPY package*.json ./
RUN apk add git && npm install

COPY . .
RUN npm run build && npm prune --production


FROM node:18-alpine

USER node
WORKDIR /app

COPY --from=build /app/lib lib/
COPY --from=build /app/migrations migrations/
COPY --from=build /app/node_modules node_modules/

CMD [ "node", "lib/index.js" ]
